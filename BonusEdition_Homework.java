package app;

import java.util.Scanner;

public class BonusEdition_Homework {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Какую сложность бы Вы хотели? Введите от 1 до 3, где 3 - самое сложное");
        int lvl_case = input.nextInt();
        switch (lvl_case) {
            case 1:
                System.out.println("Продолжите числовой ряд:");
                int number1 = 1;
                for (int i = 0; i < 5; i++) {
                    number1 *= 2;
                    System.out.print(number1);
                    System.out.print(" ");
                }
                System.out.println("?");
                int counter1 = 0;
                while (true && counter1 < 4) {
                    int num = input.nextInt();
                    counter1++;
                    if (num > 64 || num < 64) {
                        System.out.println("Попробуйте еще раз");
                    } else {
                        System.out.println("Ответ верный");
                        break;
                    }
                    if (counter1 == 4) {
                        System.out.println("Вы не угадали! Последовательность была такой: 2 4 8 16 32 64");
                    }
                }
                break;
            case 2:
                System.out.println("Продолжите числовой ряд:");
                int number2 = 5;
                for (int i = 0; i < 3; i++) {
                    number2 -= 2;
                    System.out.print(number2);
                    System.out.print(" ");
                    number2 *= 2;
                    System.out.print(number2);
                    System.out.print(" ");
                }
                System.out.println("?");
                int counter2 = 0;
                while (true && counter2 < 4) {
                    int num = input.nextInt();
                    counter2++;
                    if (num > 10 || num < 10) {
                        System.out.println("Попробуйте еще раз");
                    } else {
                        System.out.println("Ответ верный");
                        break;
                    }
                    if (counter2 == 4) {
                        System.out.println("Вы не угадали! Последовательность была такой: 3 6 4 8 6 12 10");
                    }
                }
                break;
            case 3:
                System.out.println("Продолжите числовой ряд:");
                int number3 = 10;
                for (int i = 0; i < 2; i++) {
                    number3 -= 1;
                    System.out.print(number3);
                    System.out.print(" ");
                    number3 += 3;
                    System.out.print(number3);
                    System.out.print(" ");
                    number3 /= 2;
                    System.out.print(number3);
                    System.out.print(" ");
                }
                System.out.println("?");
                int counter3 = 0;
                while (true && counter3 < 4) {
                    int num = input.nextInt();
                    counter3++;
                    if (num > 3 || num < 3) {
                        System.out.println("Попробуйте еще раз");
                    } else {
                        System.out.println("Ответ верный");
                        break;
                    }
                    if (counter3 == 4) {
                        System.out.println("Вы не угадали! Последовательность была такой: 9 12 6 5 8 4 3");
                    }
                }
                break;
            default:
                System.out.println("Похоже, что-то пошло не так! Введите число от 1 до 3!");
        }
    }
}
