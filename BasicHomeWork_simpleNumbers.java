package app;

import java.util.Scanner;

 public class BasicHomeWork_simpleNumbers {
     public static void main(String[] args) {
         System.out.println("Продолжите числовой ряд:");
         int k = 5;
         for (int i = 0; i < 3; i++) {
             k -= 2;
             System.out.print(k);
             System.out.print(" ");
             k *= 2;
             System.out.print(k);
             System.out.print(" ");
         }
         System.out.println("?");
         Scanner input = new Scanner(System.in);
         int number = input.nextInt();
         if (number==10) {
             System.out.println("Правильно, молодец!");
             } else {
             System.out.println("Попробуйте ещё");
         }
     }
 }
